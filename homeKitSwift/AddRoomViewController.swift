//
//  RoomViewController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import UIKit
import HomeKit

class AddRoomViewController : UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
  
  @IBOutlet weak var roomNameTextField: UITextField!
  @IBOutlet weak var homePickerView: UIPickerView!
  
  var homes:  [AnyObject]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(animated: Bool)  {
    // Get homes from my home list
    let homeManager = HomeManagerControllerSharedInstanced
    
    self.homes = homeManager.myHomes

    self.homePickerView.dataSource = self
    self.homePickerView.delegate   = self
    self.roomNameTextField.delegate = self
    self.roomNameTextField.inputAccessoryView = self.textToolBar()
    
    // Receive notifications
    let nc = NSNotificationCenter.defaultCenter()
    nc.addObserver(self, selector: "addRoomError:", name: kAddRoomError, object: nil)
    nc.addObserver(self, selector: "addRoomOK:", name: kAddRoomOK, object:nil)
    nc.addObserver(self, selector: "homesUpdated", name:kHomesUpdated, object:nil)

  }
  
  override func viewDidDisappear(animated: Bool) {
    NSLog("viewDidDisappear")
    let nc = NSNotificationCenter.defaultCenter()
    nc.removeObserver(self)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK:  Notifications
  func homesUpdated() {
    self.homes = HomeManagerControllerSharedInstanced.myHomes
    self.homePickerView.reloadAllComponents()
  }
  
  func addRoomError(notification:NSNotification) {
    let userInfo:Dictionary<String,String> = notification.userInfo as Dictionary<String,String>
    let messageString = userInfo["message"]!
    
    var alert = UIAlertController(title: kAppName, message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  func addRoomOK(notification:NSNotification) {
    let userInfo:Dictionary<String,String> = notification.userInfo as Dictionary<String,String>
    let messageString = userInfo["message"]!
    
    var alert = UIAlertController(title: kAppName, message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: {
      self.roomNameTextField.text = ""
    })
  }
  
  // MARK:  UITextFieldDelegate
  func textFieldShouldReturn(textField: UITextField!) -> Bool {
    ENTRY_LOG()
    textField.resignFirstResponder()
    
    self.addRoom()
    
    EXIT_LOG()
    
    return true
  }
  
  func pressedCancelButton() {
    ENTRY_LOG()
    self.roomNameTextField.resignFirstResponder()
    EXIT_LOG()
  }
  
  func addRoom() {
    let homeManager = HomeManagerControllerSharedInstanced
    
    // Get name under picker
    var row = self.homePickerView.selectedRowInComponent(0)
    var homeName = self.homes[row].name
    
    homeManager.addRoom(self.roomNameTextField.text, toHome:  homeName)
  }
  
  // MARK:  UIPickerViewDataSource
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
      return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.homes.count
  }
  
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    var home:HMHome = self.homes[row] as HMHome
    return home.name
  }
  
  // MARK:  Toolbar
  func textToolBar() -> UIToolbar {
    ENTRY_LOG()
    var toolbar:UIToolbar = UIToolbar(frame:CGRectMake(0.0, 0.0, 320.0, 44.0))
    
    var cancel:UIButton = UIButton(frame:CGRectMake(0.0, 0.0, 100.0, 44.0))
    cancel.setTitle("Cancel", forState: .Normal)
    cancel.setTitleColor(iOSBlueColor, forState: .Normal)
    cancel.addTarget(self, action: "pressedCancelButton", forControlEvents: .TouchUpInside)
    toolbar.addSubview(cancel)
    
    EXIT_LOG()
    return toolbar
  }
  
}