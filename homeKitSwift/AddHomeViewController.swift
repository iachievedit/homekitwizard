//
//  ViewController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//


import UIKit
import HomeKit

class AddHomeViewController: UIViewController, UITextFieldDelegate {
  
  @IBOutlet weak var homeNameTextField: UITextField!
  @IBOutlet weak var primaryHomeSwitch: UISwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewDidAppear(animated: Bool)  {
    super.viewDidAppear(animated)
    self.homeNameTextField.delegate = self
    self.homeNameTextField.inputAccessoryView = textToolBar()
    
    let nc = NSNotificationCenter.defaultCenter()
    nc.addObserver(self, selector: "addHomeError:", name: kAddHomeError, object: nil)
    nc.addObserver(self, selector: "addHomeOK:", name: kAddHomeOK, object:nil)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    let nc = NSNotificationCenter.defaultCenter()
    nc.removeObserver(self)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  func addHome() {
    ENTRY_LOG()
    let homeManagerController = HomeManagerControllerSharedInstanced
    
    let homeName = self.homeNameTextField.text
    
    homeManagerController.addHome(homeName, primary:self.primaryHomeSwitch.on)
    
    // Turn the primaryHomeSwitch off after adding a home so as not to confuse
    self.primaryHomeSwitch.on = false
    
    EXIT_LOG()

  }
  
  func pressedCancelButton() {
    ENTRY_LOG()
    self.homeNameTextField.resignFirstResponder()
    EXIT_LOG()
  }
  
  func textFieldShouldReturn(textField: UITextField!) -> Bool {
    ENTRY_LOG()
    
    self.addHome()
    
    textField.resignFirstResponder()
    
    EXIT_LOG()
    
    return true
  }
  
  func addHomeError(notification:NSNotification) -> Void {
    
    let userInfo:Dictionary<String,String!> = notification.userInfo as Dictionary<String,String!>
    let messageString = userInfo["message"]
    
    var alert = UIAlertController(title: kAppName, message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  func addHomeOK(notification:NSNotification) -> Void {
    
    let userInfo:Dictionary<String,String> = notification.userInfo as Dictionary<String,String>
    let messageString = userInfo["message"]!
    
    var alert = UIAlertController(title: kAppName, message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: {
      self.homeNameTextField.text = ""
      })
  }
  
  // MARK:  Toolbar
  func textToolBar() -> UIToolbar {
    ENTRY_LOG()
    var toolbar:UIToolbar = UIToolbar(frame:CGRectMake(0.0, 0.0, 320.0, 44.0))
    
    var cancel:UIButton = UIButton(frame:CGRectMake(0.0, 0.0, 100.0, 44.0))
    cancel.setTitle("Cancel", forState: .Normal)
    cancel.setTitleColor(iOSBlueColor, forState: .Normal)
    cancel.addTarget(self, action: "pressedCancelButton", forControlEvents: .TouchUpInside)
    toolbar.addSubview(cancel)
    
    EXIT_LOG()
    return toolbar
  }

}

