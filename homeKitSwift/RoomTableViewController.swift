//
//  RoomTableViewController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import UIKit
import HomeKit

class RoomTableViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {

  @IBOutlet weak var roomTableView: UITableView!
  @IBOutlet weak var homeNameLabel: UILabel!
  
  var homeName:String = "Home"
  var rooms:[String]?
  
  override func viewDidAppear(animated: Bool) {
    NSLog("viewDidAppear")
    
    roomTableView.dataSource = self
    roomTableView.delegate   = self
    
    self.homeNameLabel.text = homeName
    
    let hm    = HomeManagerControllerSharedInstanced
    self.rooms = hm.roomsForHomeNamed(homeName)
    
    
    dispatch_async(dispatch_get_main_queue(), {
      self.roomTableView.reloadData()
    })


  }
  
  override func viewDidDisappear(animated: Bool) {
    //
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // This will be the number of rooms for the home
    if self.rooms != nil {
      return self.rooms!.count
    } else {
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    NSLog("tableView cellForRowAtIndexPath")
    
    let cellIdentifier = "HomeKitCell";
    let index = indexPath.row
    
    var cell:UITableViewCell? = self.roomTableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell
    
    if cell == nil {
      cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
    }
    
    cell!.textLabel!.text = self.rooms![index]
    return cell!
  }
  
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    return true
  }

  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    
    if (editingStyle == UITableViewCellEditingStyle.Delete) {
      
      // Ask for permission to delete
      var alert = UIAlertController(title: kAppName, message: "Are you sure?  Deleting a room will remove all of the accessories in that room.", preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {
        (action:UIAlertAction!) -> Void in
        let hm = HomeManagerControllerSharedInstanced
        let index = indexPath.row
        
        var hud = JGProgressHUD(style:JGProgressHUDStyle.Dark)
        hud.textLabel.text = "Removing Room";
        hud.showInView(self.view)
        
        // Get the home
        var home:HMHome? = hm.homeForHomeNamed(self.homeName)
        if let h = home {
          
          var room:HMRoom? = h.rooms.filter({r in r.name == self.rooms![index]}).first as HMRoom?

          if let r = room {
          h.removeRoom(room, completionHandler: { (e:NSError!) -> Void in
          NSLog("Room removed")
          
          // Register for a HomesUpdated notification and then delete
          var tmpObserver:AnyObject? = nil
          tmpObserver = NSNotificationCenter.defaultCenter().addObserverForName(kHomesUpdated, object:nil, queue: nil,
            usingBlock: { (notification:NSNotification!) -> Void in
              // Update our data
              //self.homes = hm.myHomes
              
              // Remove the row
              //              self.homeKitTableView.deleteRowsAtIndexPaths([indexPath as AnyObject], withRowAnimation:UITableViewRowAnimation.Automatic)
              //              self.homeKitTableView.reloadData()
              
              hud.dismiss()
              
              // Remove our temporary observer, otherwise this block will fire
              // again when homes are updated
              NSNotificationCenter.defaultCenter().removeObserver(tmpObserver!)
            })
          
          })}
        }
        
      }))
      alert.addAction(UIAlertAction(title: "Cancel", style:UIAlertActionStyle.Cancel, handler:{
        (action:UIAlertAction!) -> Void in
        NSLog("Cancel")
      }))
      self.presentViewController(alert, animated: true, completion: nil)
    }

  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    ENTRY_LOG()
    
    if segue.identifier == "BackSegue" {
      return
    }
    
    var dvc:AccessoryTableViewController = segue.destinationViewController as AccessoryTableViewController
    
    var indexPath:NSIndexPath? = self.roomTableView.indexPathForSelectedRow()
    if let i = indexPath {
      let roomName = self.rooms![i.row]
      dvc.homeName = self.homeName
      dvc.roomName = roomName
    } else {
      SLogError("Error, no room found")
    }
  }

  
}