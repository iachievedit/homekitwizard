//
//  RoomViewController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import UIKit
import HomeKit

class HomeKitTableViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var homeKitTableView: UITableView!
  
  var homes: [AnyObject]!
  
  override func viewDidAppear(animated: Bool) {
    
    homeKitTableView.dataSource = self
    homeKitTableView.delegate   = self
    
    let hm = HomeManagerControllerSharedInstanced
    
    self.homes = hm.myHomes
    
    dispatch_async(dispatch_get_main_queue(), {
      self.homeKitTableView.reloadData()
    })
    

    
  }
  
  override func viewDidDisappear(animated: Bool) {
    //code
  }
  
  // MARK:  UITableViewDataSource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.homes.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cellIdentifier = "HomeKitCell";
    let index = indexPath.row
    
    var cell:UITableViewCell? = self.homeKitTableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell

    if cell == nil {
      cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
    }
    
    cell!.textLabel!.text = self.homes[index].name
    return cell!

  }
  
  // Override to support conditional editing of the table view.
  // This only needs to be implemented if you are going to be returning NO
  // for some items. By default, all items are editable.
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    return true
  }
  
  // Override to support editing the table view.
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    
    if (editingStyle == UITableViewCellEditingStyle.Delete) {
      
      // Ask for permission to delete
      var alert = UIAlertController(title: kAppName, message: "Are you sure?  Deleting a home will remove all rooms and accessories in that home.", preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {
        (action:UIAlertAction!) -> Void in
        let hm = HomeManagerControllerSharedInstanced
        let index = indexPath.row
        
        var hud = JGProgressHUD(style:JGProgressHUDStyle.Dark)
        hud.textLabel.text = "Removing Home";
        hud.showInView(self.view)
        
        hm.homeManager.removeHome(self.homes[index] as HMHome, completionHandler: { (e:NSError!) -> Void in
          NSLog("Home removed")
          
          // Register for a HomesUpdated notification and then delete
          var tmpObserver:AnyObject? = nil
          tmpObserver = NSNotificationCenter.defaultCenter().addObserverForName(kHomesUpdated, object:nil, queue: nil,
            usingBlock: { (notification:NSNotification!) -> Void in
              // Update our data
              self.homes = hm.myHomes
              
              // Remove the row
              self.homeKitTableView.deleteRowsAtIndexPaths([indexPath as AnyObject], withRowAnimation:UITableViewRowAnimation.Automatic)
              self.homeKitTableView.reloadData()
              
              hud.dismiss()
              
              // Remove our temporary observer, otherwise this block will fire
              // again when homes are updated
              NSNotificationCenter.defaultCenter().removeObserver(tmpObserver!)
          })
          
           }
        )
        
        }))
      alert.addAction(UIAlertAction(title: "Cancel", style:UIAlertActionStyle.Cancel, handler:{
        (action:UIAlertAction!) -> Void in
        NSLog("Cancel")
      }))
      self.presentViewController(alert, animated: true, completion: nil)
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    ENTRY_LOG()
    
    SLogVerbose("\(segue.identifier)")
    if segue.identifier == "BackSegue" {
        return
    }
    
    var dvc:RoomTableViewController = segue.destinationViewController as RoomTableViewController
    
    var indexPath:NSIndexPath? = self.homeKitTableView.indexPathForSelectedRow()
    if let i = indexPath {
      let homeName = self.homes[i.row].name
      dvc.homeName = homeName
    } else {
      SLogError("Error, no home found")
    }


    
  }
}

