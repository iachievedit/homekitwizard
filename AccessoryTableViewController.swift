//
//  AccessoryTableViewController.swift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import UIKit
import HomeKit

class AccessoryTableViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var accessoryTableView: UITableView!
  @IBOutlet weak var roomNameLabel: UILabel!
  
  var homeName:String = "Home"
  var roomName:String = "Room"
  
  var accessories:[String]?
  
  override func viewDidAppear(animated: Bool) {
    ENTRY_LOG()
    
    accessoryTableView.dataSource = self
    accessoryTableView.delegate   = self
    
    self.roomNameLabel.text = roomName
    
    let hm    = HomeManagerControllerSharedInstanced
    self.accessories = hm.accessoriesForRoomNamed(roomName, inHome:homeName)
    
    dispatch_async(dispatch_get_main_queue(), {
      self.accessoryTableView.reloadData()
    })
    
    EXIT_LOG()
    
    
  }
  
  override func viewDidDisappear(animated: Bool) {
    //
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // This will be the number of accessories assigned to the room
    if self.accessories != nil {
      return self.accessories!.count
    } else {
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    SLogVerbose("tableView cellForRowAtIndexPath")
    
    let cellIdentifier = "HomeKitCell";
    let index = indexPath.row
    
    var cell:UITableViewCell? = self.accessoryTableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell
    
    if cell == nil {
      cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
    }
    
    cell!.textLabel!.text = self.accessories![index]
    return cell!
  }
  
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    return true
  }
  
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    ENTRY_LOG()
    
    var dvc:RoomTableViewController = segue.destinationViewController as RoomTableViewController
    
    dvc.homeName = self.homeName

  }

  
}